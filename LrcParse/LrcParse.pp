unit LrcParse;
{$mode delphi}
interface

uses classes;
	type LrcS=record
		    mm:longint;  //分钟
		    ss:longint;	 //秒
		    ms:longint;	 //毫秒
		    ci:ansistring; //歌词
		   end;

	//解释歌词文件(path)
	function parseLrc(path:ansistring):TList;

	//释放歌词
	procedure freeGeCi(list:TList);

implementation
uses RegExpr,sysutils;
const 
	geci='(\[\d+:[0-5]\d(\.\d+)?\])+(.*)';	//歌词
	gecitime='\[(\d+):([0-5]\d)(\.(\d+))?\]';	//歌词时间

	function compare(x,y:pointer):integer;forward;
	//解释歌词文件(path)

	function parseLrc(path:ansistring):TList;
	var
		list:TList;
		x:text;
		y:ansistring;
		ci,t:ansistring;
		r:TRegExpr;
		p:^LrcS;
	begin
		list:=TList.create;

		r:=TRegExpr.Create;	//创建一个正规表达式对象
		assign(x,path);	//打开歌词文件		
		try
			
			reset(x);
			//writeln(GetTextCodePage(x));
			while not eof(x) do
			begin
				readln(x,y);
				//writeln(y);
				r.Expression:=geci; //匹配歌词
				ci:='';
				if r.Exec(y) then
				begin
					ci:=r.Match[3];		//每一行的歌词
					//writeln(ci);
					r.Expression:=gecitime;
					
					if r.exec(y) then
					begin
						repeat
							//t:=r.Match[0]; //时间
							//writeln(r.Match[0]+ci);//时间 + 歌曲
							//writeln(r.Match[1],':',r.Match[2],'.',r.Match[4]); //[1]分,[2]秒,[4]毫秒
							new(p);
							p^.mm:=strtoint(r.Match[1]);
							p^.ss:=strtoint(r.Match[2]);
							p^.ms:=strtoint(r.Match[4]);
							p^.ci:=ci;
							//writeln( '[', p^.mm ,':',p^.ss,'.',p^.ms,']',p^.ci);
							list.add(p);
						until not r.ExecNext;
					end;
					
				end;
			end;
			//对歌词数组排序
		finally 
			close(x);
			//r.Free;
		end;
		list.sort(@compare);
		exit(list);
	end;

	{
	Value Description

	> 0 (positive) Item1 is less than Item2
	   0 Item1 is equal to Item2
	< 0 (negative) Item1 is greater than Item2
	}
	function compare(x,y:pointer):integer;
	var
		x1,y1:^lrcS;
		a,b:lrcS;
	begin
		x1:=x;
		y1:=y;
		a:=x1^;
		b:=y1^;		
		if ( (a.mm*60+a.ss) > (b.mm*60+b.ss)) then
			exit(1)
		else if ( (a.mm*60+a.ss) < (b.mm*60+b.ss)) then
			exit(-1)
		else if (a.ms>b.ms) then
			exit(1)
		else if(a.ms<b.ms)then
			exit(-1)
		else
			exit(0);
	end;

	procedure freeGeCi(list:TList);
	var
		i,len:longint;
		p:^lrcS;
	begin
		len:=list.count;
		for i:=0 to len-1 do
		begin
			p:=list.items[i];
			dispose(p);
		end;
		list.clear();
		list.Destroy();
	end;	
end.