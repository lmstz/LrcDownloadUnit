unit LrcDownload;
{$mode objFPC}

interface

type
	LrcInfo=record
			aid:longint;		//aid
			lrc:string;		//歌词的地址
			song:string;		//歌名 utf8
			artist_id:longint;	//艺术家id
			artist:string;		//艺术家名
			sid:longint;		//sid
		end;

	LrcInfoList=array of LrcInfo;


	//搜索歌词 (歌名)
	function searchLrc(song : string ):ansistring;

	//搜索歌词 (歌名, 艺术家)
	function searchLrc(song, artist : string):ansistring;

	//取艺术家的名字 （ID)
	function getArtistName(artist_id:longint):string;

	// 下载歌词( sid )
	function downLrc(sid:longint):ansistring;

	// 下载歌词( sid,保存地址 )
	function downLrc(sid:longint;path:string):ansistring;

	//下载歌词( 网址 )
	function downLrc(url:string):ansistring;

	//下载歌词( 网址,保存地址 )
	function downLrc(url,path:string):ansistring;

	//取LRC列表(ansistring)
	//getLrcInfoList(ansistring)
	function getLrcInfoList(s:ansistring):LrcInfoList;

	//释放歌词信息列表
	procedure freeLrcInfoList(x:LrcInfoList);

implementation
uses fphttpclient,SysUtils, fpjson, jsonparser;
type	UTF8String = type AnsiString(CP_UTF8);

	function getLrcURLByID(sid:longint):string;forward;
	function URLEncode(const S: string; const InQueryString: Boolean): string;forward;
	function httpGet(url:string):ansistring; forward;

	//搜索歌词 (歌名)
	function searchLrc(song : string ):ansistring;
	var
		s:ansistring;
	begin

		s := httpGet('http://gecimi.com/api/lyric/'+URLEncode(UTF8Encode(Song),false));
		exit(s);
	end;

	//根据歌词 (歌名, 艺术家)
	function searchLrc(song, artist : string):ansistring;
	var
		s:ansistring;
		url:string;		
	begin
		url:='http://gecimi.com/api/lyric/'+URLEncode(UTF8Encode(Song),false)+'/'+URLEncode(UTF8Encode(artist),false);
		s := httpGet(url);
		exit(s);
	end;

	//取艺术家的名字 （artist_id)
	function getArtistName(artist_id:longint):string;
	var
		s:string;
		t:ansistring;
		D,E: TJSONData;
	begin
		s := httpGet('http://gecimi.com/api/artist/'+inttostr(artist_id));
		D := GetJson(s);
		E:=D.FindPath('count');
		t:=E.asJSON;
		if t='0' then
			s:='没有此歌手'
		else
		begin
			t:=D.FindPath('result.name').asJSON;
			s:=copy(t,2,length(t)-2);
		end;
		exit(s);
	end;


	function getLrcURLByID(sid:longint):string;
	var
		s:ansistring;
		t:ansistring;
		D,E: TJSONData;
	begin
		s:=httpGet('http://gecimi.com/api/lrc/'+inttostr(sid));
		D:=GetJson(s);
		E:=D.FindPath('count');
		t:=E.asJSON;
		if t='0' then
			s:='没有这个id对应的歌词'
		else
		begin
			t:=D.FindPath('result.lrc').asJSON;
			s:=copy(t,2,length(t)-2);
			s:=StringReplace(s,'\/','/',[rfReplaceAll]);
		end;
		exit(s);
	end;

	// 下载歌词( sid )
	function downLrc(sid:longint):ansistring;
	var
		s:ansistring;
	begin
		s:=getLrcURLByID(sid);
		s:=downLrc(s);
		exit(s);
	end;

	// 下载歌词( sid,保存地址 )
	function downLrc(sid:longint;path:string):ansistring;
	var
		s:ansistring;
	begin
		s:=getLrcURLByID(sid);
		s:=downLrc(s,path);
		exit(s);
	end;

	//下载歌词( 网址 )
	function downLrc(url:string):ansistring;
	var
		s:ansistring;
	begin
		s := httpGet(url);
		exit(s);
	end;

	//下载歌词( 网址,保存地址 )
	function downLrc(url,path:string):ansistring;
	var
		s:ansistring;
		outfile: Text;
	begin
		s:=downLrc(url);
		assign(outfile,path);
		rewrite(outfile);
		writeln(outfile,s);
		close(outfile);
		exit(s);
	end;


	//取LRC列表(ansistring)
	//getLrcInfoList(ansistring)
	function getLrcInfoList(s:ansistring):LrcInfoList;
	var
		count,i:longint;
		D,E: TJSONData;
		list: LrcInfoList;
		t:ansistring;
	begin
		D:=GetJson(s);
		E:=D.FindPath('count');
		t:=E.asJSON;
		count:=strtoint(t);
		if count=0 then
			setLength(list,0)
		else
		begin
			setLength(list,count);
			for i:=0 to count-1 do
			begin
				//aid
				list[i].aid:=strtoint(D.FindPath('result['+inttostr(i)+'].aid').asJSON);

				//lrc
				t:=D.FindPath('result['+inttostr(i)+'].lrc').asJSON;
				t:=copy(t,2,length(t)-2);
				t:=StringReplace(t,'\/','/',[rfReplaceAll]);
				list[i].lrc:=t;

				//song
				list[i].song:=D.FindPath('result['+inttostr(i)+'].song').asJSON;

				//artist_id
				list[i].artist_id:=strtoint(D.FindPath('result['+inttostr(i)+'].artist_id').asJSON);

				//artist
				
				try
					list[i].artist:=getArtistName(list[i].artist_id);
				except
					list[i].artist:='';
				end;
				
				//sid
				list[i].sid:=strtoint(D.FindPath('result['+inttostr(i)+'].sid').asJSON);

			end;
		end;
		exit(list);
	end;

	//释放歌词信息列表
	procedure freeLrcInfoList(x:LrcInfoList);
	begin
		setLength(x,0);
		x:=nil;
	end;

	//URLEncode();
	function URLEncode(const S: string; const InQueryString: Boolean): string;
	var
	  Idx: Integer; // loops thru characters in string
	  MyResult:string;
	begin
	  MyResult := '';
	  for Idx := 1 to Length(S) do
	  begin
	    case S[Idx] of
	      'A'..'Z', 'a'..'z', '0'..'9', '-', '_', '.':
		MyResult := MyResult + S[Idx];
	      ' ':
		if InQueryString then
		  MyResult := MyResult + '+'
		else
		  MyResult := MyResult + '%20';
	      else
		MyResult := MyResult + '%' + SysUtils.IntToHex(Ord(S[Idx]), 2);
	    end;
	  end;
	  exit(MyResult);
	end;

	function httpGet(url:string):ansistring;
	var
		s:ansistring;
	begin
		try
			s:=TFPCustomHTTPClient.SimpleGet(url);
		except
			s:='网络错误';
		end;
		exit(s);
	end;
end.