{$codepage utf8}
uses KLrcDownload;

var
    l:LrcInfoList;
    i:longint;
begin
    l:=searchLrc('我们不一样','大壮');
    for i:=0 to length(l)-1 do
    begin
        if l[i].album_id<>'' then
        begin
            downSongImg(l[i], utf8encode('我们不一样.jpg'));
            downLrc(l[i],utf8encode('我们不一样.lrc'));
            downSong(l[i],utf8encode('我们不一样.mp3'));
            break;
        end;
    end;
    freeLrcInfoList(l);

    l:=searchLrc('Mình Yêu Nhau Đi');
    for i:=0 to length(l)-1 do
    begin
        if l[i].album_id<>'' then
        begin
            downSongImg(l[i], utf8encode('Mình Yêu Nhau Đi.jpg'));
            downLrc(l[i],utf8encode('Mình Yêu Nhau Đi.Lrc'));
            downSong(l[i],utf8encode('Mình Yêu Nhau Đi.mp3'));
            break;
        end;
    end;
    freeLrcInfoList(l);
end.